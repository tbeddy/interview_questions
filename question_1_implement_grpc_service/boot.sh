#!/bin/sh

set -e

# Construct Docker image
docker image build -t recipe_server .

# Stand up image
docker container run -p 50051:50051 --detach --name recipe recipe_server

{
    # Run test battery
    pipenv run pytest src/test/test_recipe.py -v
    # Remove container once testing is over
    docker stop recipe
    docker rm recipe
} || {
    docker rm recipe
}
