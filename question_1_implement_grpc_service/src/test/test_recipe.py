import grpc
import pytest

from src.lib.RecipeAPI_pb2_grpc import RecipeAPIStub
from src.lib.RecipeAPI_pb2 import (
    RecipeItem,
    RecipeCreateRequest,
    RecipeListAllRequest,
    RecipeListAllResponse,
    RecipeGetByNameRequest,
    RecipeListByIngredientsRequest
)

@pytest.fixture()
def recipe_client():
    channel = grpc.insecure_channel('localhost:50051')
    stub = RecipeAPIStub(channel)
    return stub

# RecipeCreate tests

def test_recipe_create(recipe_client):
    ppg_recipe_name = "powerpuff girls"
    ppg_ingredients = [RecipeItem(ingredient_name = "sugar",
                                  measurement = "1 cup"),
                       RecipeItem(ingredient_name = "spice",
                                  measurement = "1 cup"),
                       RecipeItem(ingredient_name = "everything nice",
                                  measurement = "1 cup"),
                       RecipeItem(ingredient_name = "chemical x",
                                  measurement = "too much")]

    try:
        request = RecipeCreateRequest(recipe_name = ppg_recipe_name)
        request.ingredients.extend(ppg_ingredients)
        result = recipe_client.RecipeCreate(request)

        assert result.successfully_stored_recipe
    except grpc.RpcError as e:
        assert False

def test_recipe_create_invalid_name(recipe_client):

    ppg_recipe_name = None
    ppg_ingredients = [RecipeItem(ingredient_name = "sugar",
                                  measurement = "1 cup"),
                       RecipeItem(ingredient_name = "spice",
                                  measurement = "1 cup")]

    try:
        request = RecipeCreateRequest(recipe_name = ppg_recipe_name)
        request.ingredients.extend(ppg_ingredients)
        result = recipe_client.RecipeCreate(request)
    except grpc.RpcError as e:
        assert 'The recipe_name argument was empty' in e.details()
        assert e.code() == grpc.StatusCode.INVALID_ARGUMENT

def test_recipe_create_invalid_ingredient_name(recipe_client):

    ppg_recipe_name = "powerpuff girls"
    ppg_ingredients = [RecipeItem(ingredient_name = None,
                                  measurement = "1 cup"),
                       RecipeItem(ingredient_name = "spice",
                                  measurement = "1 cup")]

    try:
        request = RecipeCreateRequest(recipe_name = ppg_recipe_name)
        request.ingredients.extend(ppg_ingredients)
        result = recipe_client.RecipeCreate(request)
    except grpc.RpcError as e:
        assert 'At least one RecipeItem within the ingredients argument had an empty ingredient_name argument' in e.details()
        assert e.code() == grpc.StatusCode.INVALID_ARGUMENT

def test_recipe_create_invalid_ingredient_measurement(recipe_client):

    ppg_recipe_name = "powerpuff girls"
    ppg_ingredients = [RecipeItem(ingredient_name = "sugar",
                                  measurement = None),
                       RecipeItem(ingredient_name = "spice",
                                  measurement = "1 cup")]

    try:
        request = RecipeCreateRequest(recipe_name = ppg_recipe_name)
        request.ingredients.extend(ppg_ingredients)
        result = recipe_client.RecipeCreate(request)
    except grpc.RpcError as e:
        assert 'At least one RecipeItem within the ingredients argument had an empty measurement argument' in e.details()
        assert e.code() == grpc.StatusCode.INVALID_ARGUMENT

def test_recipe_create_repeated_recipe_name(recipe_client):

    test_recipe_name1 = "recipe1"
    test_ingredients1 = [RecipeItem(ingredient_name = "I",
                                    measurement = "should"),
                         RecipeItem(ingredient_name = "be",
                                    measurement = "fine")]

    request = RecipeCreateRequest(recipe_name = test_recipe_name1)
    request.ingredients.extend(test_ingredients1)
    recipe_client.RecipeCreate(request)

    test_recipe_name2 = "recipe1"
    test_ingredients2 = [RecipeItem(ingredient_name = "But",
                                    measurement = "I"),
                         RecipeItem(ingredient_name = "should",
                                    measurement = "fail")]

    try:
        request = RecipeCreateRequest(recipe_name = test_recipe_name2)
        request.ingredients.extend(test_ingredients2)
        result = recipe_client.RecipeCreate(request)
    except grpc.RpcError as e:
        assert 'That recipe_name is already in the system' in e.details()
        assert e.code() == grpc.StatusCode.ALREADY_EXISTS
        
# RecipeListAll tests

def test_recipe_list_all(recipe_client):
    recipe_request1 = RecipeCreateRequest(recipe_name = "foo")
    recipe_request1.ingredients.append(RecipeItem(ingredient_name = "a",
                                                  measurement = "1"))
    recipe_client.RecipeCreate(recipe_request1)

    recipe_request2 = RecipeCreateRequest(recipe_name = "bar")
    recipe_request2.ingredients.append(RecipeItem(ingredient_name = "b",
                                                  measurement = "2"))
    recipe_client.RecipeCreate(recipe_request2)

    request = RecipeListAllRequest()
    result = recipe_client.RecipeListAll(request)

    correct_response = RecipeListAllResponse()
    # Relies on earlier test; probably should be rewritten
    correct_response.recipe_names.extend(["powerpuff girls", "recipe1", "foo", "bar"])
    assert result.recipe_names == correct_response.recipe_names

# RecipeGetByName tests

def test_recipe_get_by_name(recipe_client):
    test_name = "find me"
    test_ingredients = [RecipeItem(ingredient_name = "a",
                                   measurement = "1"),
                        RecipeItem(ingredient_name = "b",
                                   measurement = "2")]
    recipe_request1 = RecipeCreateRequest(recipe_name = test_name)
    recipe_request1.ingredients.extend(test_ingredients)
    recipe_client.RecipeCreate(recipe_request1)

    try:
        result = recipe_client.RecipeGetByName(RecipeGetByNameRequest(name_to_get = "find me"))
        assert result.name == test_name
        for correct_recipe, result_recipe in zip(test_ingredients, result.ingredients):
            assert correct_recipe == result_recipe
    except grpc.RpcError as e:
        assert False

def test_recipe_get_by_name_recipe_not_found(recipe_client):
    try:
        recipe_client.RecipeGetByName(RecipeGetByNameRequest(name_to_get = "don't find me"))
    except grpc.RpcError as e:
        assert 'The recipe could not be found' in e.details()
        assert e.code() == grpc.StatusCode.NOT_FOUND

# RecipeListByIngredients tests

def test_recipe_list_by_ingredients(recipe_client):
    recipe_request1 = RecipeCreateRequest(recipe_name = "purple")
    recipe_request1.ingredients.extend([RecipeItem(ingredient_name = "red",
                                                   measurement = "1"),
                                        RecipeItem(ingredient_name = "blue",
                                                   measurement = "1")])
    recipe_client.RecipeCreate(recipe_request1)

    recipe_request2 = RecipeCreateRequest(recipe_name = "orange")
    recipe_request2.ingredients.extend([RecipeItem(ingredient_name = "red",
                                                   measurement = "1"),
                                        RecipeItem(ingredient_name = "yellow",
                                                   measurement = "1")])
    recipe_client.RecipeCreate(recipe_request2)

    recipe_request3 = RecipeCreateRequest(recipe_name = "pink")
    recipe_request3.ingredients.extend([RecipeItem(ingredient_name = "white",
                                                   measurement = "1"),
                                        RecipeItem(ingredient_name = "red",
                                                   measurement = "1")])
    recipe_client.RecipeCreate(recipe_request3)

    recipe_request4 = RecipeCreateRequest(recipe_name = "green")
    recipe_request4.ingredients.extend([RecipeItem(ingredient_name = "yellow",
                                                   measurement = "1"),
                                        RecipeItem(ingredient_name = "blue",
                                                   measurement = "1")])
    recipe_client.RecipeCreate(recipe_request4)

    recipe_request5 = RecipeCreateRequest(recipe_name = "light orange")
    recipe_request5.ingredients.extend([RecipeItem(ingredient_name = "red",
                                                   measurement = "1"),
                                        RecipeItem(ingredient_name = "yellow",
                                                   measurement = "1"),
                                        RecipeItem(ingredient_name = "white",
                                                   measurement = "1")])
    recipe_client.RecipeCreate(recipe_request5)

    try:
        request = RecipeListByIngredientsRequest()
        request.ingredients_to_get.append("red")
        result = recipe_client.RecipeListByIngredients(request)
        assert result.recipe_names == ["purple", "orange", "pink", "light orange"]

        request = RecipeListByIngredientsRequest()
        request.ingredients_to_get.append("blue")
        result = recipe_client.RecipeListByIngredients(request)
        assert result.recipe_names == ["purple", "green"]

        request = RecipeListByIngredientsRequest()
        request.ingredients_to_get.extend(["red", "white"])
        result = recipe_client.RecipeListByIngredients(request)
        assert result.recipe_names == ["pink", "light orange"]
    except grpc.RpcError as e:
        assert False

def test_recipe_list_by_ingredients_invalid_ingredients(recipe_client):
    try:
        request = RecipeListByIngredientsRequest()
        recipe_client.RecipeListByIngredients(request)
    except grpc.RpcError as e:
        assert 'The ingredients_to_get argument was empty' in e.details()
        assert e.code() == grpc.StatusCode.INVALID_ARGUMENT
