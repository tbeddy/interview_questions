"""server.py
This file implements the proto file found at src/lib/RecipeAPI.proto.
"""

import grpc
from concurrent import futures
from google.protobuf.message import Message

from lib.RecipeAPI_pb2 import (
    RecipeItem,
    RecipeCreateRequest,
    RecipeCreateResponse,
    RecipeListAllRequest,
    RecipeListAllResponse,
    RecipeGetByNameRequest,
    RecipeGetByNameResponse,
    RecipeListByIngredientsRequest,
    RecipeListByIngredientsResponse
)

from lib.RecipeAPI_pb2_grpc import (
    add_RecipeAPIServicer_to_server,
    RecipeAPIServicer
)

# All recipes are stored here
recipes = {}

class RecipeAPI(RecipeAPIServicer):
    """A server that stores and retrieves food recipes.
    """

    @staticmethod
    def _report_request(method_name: str, request: Message) -> None:
        """Reports some info to stdout.

        Lifted entirely from https://gitlab.com/pairity/interview_questions/blob/master/question_2_dockerize_grpc_service/src/server.py

        Args:
            method_name: the name of the method being reported.
            request: the request to report.
        """
        print(
            f'Request was to {method_name} with params: \n {request}'
        )

    def RecipeCreate(self,
                     request: RecipeCreateRequest,
                     context: grpc.ServicerContext
                     ) -> RecipeCreateResponse:
        """Creates a recipe from received instructions.

        Recipe is not recoverable between server restarts.

        Args:
            request: the request.
            context: an RPC context.

        Returns:
            Confirmation that the recipe was successfully created
            (bool in response object).
        """
        self._report_request(self.RecipeCreate.__name__, request)
        if not request.recipe_name:
            context.set_code(grpc.StatusCode.INVALID_ARGUMENT)
            context.set_details('The recipe_name argument was empty')
        elif any(not r.ingredient_name for r in request.ingredients):
            context.set_code(grpc.StatusCode.INVALID_ARGUMENT)
            context.set_details('At least one RecipeItem within the ingredients argument had an empty ingredient_name argument')
        elif any(not r.measurement for r in request.ingredients):
            context.set_code(grpc.StatusCode.INVALID_ARGUMENT)
            context.set_details('At least one RecipeItem within the ingredients argument had an empty measurement argument')
        elif request.recipe_name in recipes.keys():
            context.set_code(grpc.StatusCode.ALREADY_EXISTS)
            context.set_details('That recipe_name is already in the system')
        else:
            recipes[request.recipe_name] = request.ingredients
            return RecipeCreateResponse(successfully_stored_recipe = True)

    def RecipeListAll(self,
                      request: RecipeListAllRequest,
                      context: grpc.ServicerContext
                      ) -> RecipeListAllResponse:
        """Returns a list of all recipes stored during current session.

        Args:
            request: the request.
            context: an RPC context.

        Returns:
            List of recipe names
            (list of strings in response object).
        """
        self._report_request(self.RecipeListAll.__name__, request)
        response = RecipeListAllResponse()
        for r in recipes.keys():
            response.recipe_names.append(r)
        return response

    def RecipeGetByName(self,
                        request: RecipeGetByNameRequest,
                        context: grpc.ServicerContext
                        ) -> RecipeGetByNameResponse:
        """Retrieve a recently stored recipe by name.

        Args:
            request: the request.
            context: an RPC context.

        Returns:
            Recipe name and its ingredients
            (string and list of RecipeItems in response object)
        """
        self._report_request(self.RecipeGetByName.__name__, request)
        if request.name_to_get in recipes.keys():
            response = RecipeGetByNameResponse(name = request.name_to_get)
            response.ingredients.extend(recipes[request.name_to_get])
            return response
        else:
            context.set_code(grpc.StatusCode.NOT_FOUND)
            context.set_details('The recipe could not be found')
            return RecipeGetByNameResponse()

    def RecipeListByIngredients(self,
                               request: RecipeListByIngredientsRequest,
                               context: grpc.ServicerContext
                               ) -> RecipeListByIngredientsResponse:
        """Retrieve recently stored recipes by ingredient(s).

        Args:
            request: the request.
            context: an RPC context.

        Returns:
            List of recipe names
            (list of strings in response object).
        """
        self._report_request(self.RecipeListByIngredients.__name__, request)
        if not request.ingredients_to_get:
            context.set_code(grpc.StatusCode.INVALID_ARGUMENT)
            context.set_details('The ingredients_to_get argument was empty')
        else:
            # Check each recipe to see if it contains all needed ingredients
            matching_recipes = {k: v for k, v in recipes.items()
                                if all(needed_ingredient in [i.ingredient_name for i in v]
                                       for needed_ingredient in request.ingredients_to_get)}
            response = RecipeListByIngredientsResponse()
            response.recipe_names.extend(list(matching_recipes))
            return response

if __name__ == "__main__":
    server = grpc.server(futures.ThreadPoolExecutor(max_workers=10))
    add_RecipeAPIServicer_to_server(RecipeAPI(), server)
    server.add_insecure_port('[::]:50051')
    server.start()
    print('Server has started!!!')
    server.wait_for_termination()
