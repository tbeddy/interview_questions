# Coding Interview Questions:

Contained in the project are two subdirectories each with a question in it.

We expect attempts at both.

The questions overview:
 - Implement a GRPC service in Python.
 - Dockerize a GRPC service.


Both have different Pipfiles, so do your work in each subdir, not this parent
directory.
