#!/bin/sh

set -e

# Construct Docker image
docker image build -t echo_server .

# Stand up image
docker container run -p 50051:50051 --detach --name echo echo_server

{
    # Run test battery
    pipenv run pytest src/test/test_echo.py -v
    # Remove container once testing is over
    docker stop echo
    docker rm echo
} || {
    docker rm echo
}
