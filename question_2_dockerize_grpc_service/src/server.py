"""server.py

for reference: https://grpc.io/docs/tutorials/basic/python/

This is a basic echo server.

Can simply be run from the `Question_2` directory.

For proto reference, check `src/lib/echo.proto`
"""

from concurrent import futures
from uuid import uuid4

from google.protobuf.message import Message
from lib.echo_pb2_grpc import (
    EchoAPIServicer,
    add_EchoAPIServicer_to_server
)

from lib.echo_pb2 import (
    EchoInputRequest,
    EchoInputResponse,
    EchoRandomNoiseRequest,
    EchoRandomNoiseResponse
)

import grpc


class EchoAPI(EchoAPIServicer):
    """An echo server.
    """

    @staticmethod
    def _report_request(method_name: str, request: Message) -> None:
        """Reports some info to stdout.

        Args:
            method_name: the name of the method being reported.
            request: the request to report.
        """
        print(
            f'Request was to {method_name} with params: \n {request}'
        )

    def EchoInput(self, request: EchoInputRequest, context: grpc.ServicerContext) -> EchoInputResponse:
        """Echos some input with optional prefix and suffix.

        Args:
            request: the request.
            context: an RPC context.

        Returns:
            The concat'ed response.
        """
        self._report_request(self.EchoInput.__name__, request)
        if not request.user_input:
            context.set_code(grpc.StatusCode.INVALID_ARGUMENT)
            context.set_details('The user_input field on the request was empty!')
            return EchoInputResponse()

        return EchoInputResponse(
            user_input_echoed=request.user_provided_prefix + request.user_input + request.user_provided_suffix
        )

    def EchoRandomNoise(self,
                        request: EchoRandomNoiseResponse,
                        context: grpc.ServicerContext
                        ) -> EchoRandomNoiseResponse:
        """Returns a random UUID.

        Args:
            request: A request.
            context: an RPC context.

        Returns:
            A response object containing a UUID.
        """
        self._report_request(self.EchoRandomNoise.__name__, request)
        return EchoRandomNoiseResponse(
            random_noise=str(uuid4())
        )


if __name__ == "__main__":
    server = grpc.server(futures.ThreadPoolExecutor(max_workers=10))
    add_EchoAPIServicer_to_server(EchoAPI(), server)
    server.add_insecure_port('[::]:50051')
    server.start()
    print('Server has started!!!')
    server.wait_for_termination()
